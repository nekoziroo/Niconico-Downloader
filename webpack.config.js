module.exports = {
  entry: {
    background: './src/background/main.ts',
    content: './src/content_scripts/main.ts',
    'options/options': './src/options/main.ts',
  },
  output: {
    path: `${__dirname}/dist`,
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
      },
    ],
  },
  resolve: {
    extensions: ['.ts'],
  },
};
