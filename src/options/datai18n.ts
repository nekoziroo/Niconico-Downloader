export default class Datai18n {
  private attributes: string[];
  constructor(attributes?: string[]) {
    this.attributes = attributes;
  }

  public render(): void {
    const data = this.getDataElement();
    data.forEach((element) => {
      const i18n = browser.i18n.getMessage(element.dataset.i18n);
      element.insertAdjacentText('afterbegin', i18n);
    });

    this.attributes.forEach((attribute) => {
      const dataElements = document.querySelectorAll(`[data-i18n-${attribute}]`);

      Array.from(dataElements).forEach((element) => {
        const i18n = browser.i18n.getMessage(element.getAttribute(`data-i18n-${attribute}`));
        element.setAttribute(attribute, i18n);
      });
    });
  }

  private getDataElement(): HTMLDataElement[] {
    const dataName = '[data-i18n]';
    const data = document.querySelectorAll(dataName);

    return [...data] as HTMLDataElement[];
  }
}
